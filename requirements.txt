redis==2.10.6
coralillo==0.6.8
blinker==1.4
jinja2==2.9.6
psycopg2==2.7.3.1
simplejson==3.11.1
requests==2.18.4
pytz
